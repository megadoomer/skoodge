/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Authentication and Authorization handleing for the alice platform
 * @module skoodge
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires skoodge/events 
 * @requires skoodge/commands 
 * @requires skoodge/models 
 * @requires skoodge/lib 
 */


module.exports = require('./events');
module.exports.getUserModel = require('./lib/loading').getUserModel;
