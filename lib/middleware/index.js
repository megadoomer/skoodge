/*jshint node:true, laxcomma: true, smarttabs: true, esnext:true */
'use strict';
/**
 * <DESCRIPTION>
 * @module skoodge/lib/middleware
 * @author Eric satterwhite
 * @since 0.0.0
 * @requires passport
 */
var passport = require( 'passport' )
var logger   = require('bitters')
var _passport = new passport.Passport();

_passport.use(require('./authentication/apikey'));
_passport.use(require('./authentication/jwtbearer'));
// user is tokenPayload from above
// because their are no session cookies here.
_passport.serializeUser(function (user, done) {
    logger.info('serializing user', user)
    done(null, user);
});

_passport.deserializeUser(function (user, done) {
    logger.debug("deserializing user")
    done(null, user);
});
// FIXME: There needs to be a way to relay error messages instead of the text "unauthorized"
// spiritshop-api/node_modules/passport/lib/middleware/authenticate.js
exports.AuthenticationMiddleware = _passport.authenticate(['localapikey', 'jwtbearer']);

// re-expose passorts initialize middleware, which has to be called
exports.initialize = _passport.initialize.bind( _passport )
