/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Handles HTTP Bearer authentication. If you have one, anything goes!
 * @module hive-auth/lib/middleware/authentication/jwtbearer
 * @author Eric Satterwhite
 * @author Samaj Shekhar
 * @since 0.1.0
 * @requires passport
 * @requires hive-conf
 * @requires hive-auth/lib/token
 */

 var conf            = require( 'keef' )                         // standard config loader
   , BearerStrategy  = require('passport-http-bearer').Strategy  // npm BearerStrategy for pass port
   , tokenExipration = conf.get('jwtToken:hours')                // configured token timeout in hourse
   , attempt         = require('gaz/function').attempt           // standard function attempt method
   , logger          = require('bitters')                        // standard logger module
   , token           = require('../../token')                    // npm jwt-simple for generating and digesting jwts
   , token           = require('../../token')                    // internal function to generate a user auth token object
   , jwtsecret       = conf.get('jwtToken:secret')               // configured secret uuid for validating jwts
   ;


module.exports = new BearerStrategy( function verify( authtoken, done ){

	// try/catch wrapper for v8 optimistions
	attempt(
		function(){
			var decoded = token.decode( authtoken, jwtsecret );
			logger.info('attempt to authenticate with bearer web token', decoded )
			if(decoded.expiry >= (+ new Date() ) ){
				logger.warning('bearer web token access: granted' )
				return done( null, decoded)
			}
			logger.warning('bearer web token access: denied' )
			throw new Error();
		}
	  , function(){
	  		logger.error("authentication web token expired")
		  	return done(null, false,{message: 'Expired token'})
	  }
	)
})

module.exports.name = 'jwtbearer'

