/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Handles straigh apikey authentication. If you have one, anything goes!
 * @module skoodge/lib/middleware/authentication/apikey
 * @author Eric Satterwhite
 * @author Samaj Shekhar
 * @since 0.1.0
 * @requires passport
 * @requires keef
 * @requires skoodge/lib/token
 */

 var conf = require( 'keef' )
   , logger = require('bitters')
   , APIKeyStrategy = require('passport-localapikey').Strategy
   , tokenExipration = conf.get('jwtToken:hours')
   , token = require('../../token')
   ;


module.exports = new APIKeyStrategy( function verify( key, done ){
	logger.info("attempted access by api key")
	if( !conf.get('affiliateApiKeys:' + key ) ){
		logger.warning('access to key %s: denied', key)
		return done(null, false, {message: 'unknown apikey'})
	}
	logger.info('access to key %s: granted', key)
	return done( null, token.tokenize( key, 'read') );
})

