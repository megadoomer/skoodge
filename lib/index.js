/*jshint node:true, laxcomma: true, smarttabs: true*/
'use strict';
/**
 * Entry point for auth module
 * @module hive-auth/lib
 * @author Eric satterwhite
 * @since 0.1.0
 * @requires hive-auth/lib/middleware
 * @requires hive-auth/lib/token
 */


exports.middleware = require("./middleware")
exports.token = require('./token')
