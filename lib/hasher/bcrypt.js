/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * bcrypt.js
 * @module bcrypt.js
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires util
 * @requires bcrypt-nodejs
 * @requires crypto
 * @requires gaz/class
 * @requires gaz/class/options
 * @requires gaz/class/parent
 * @requires skoodge/lib/hasher/default
 */

var util    = require( 'util' )
  , bcrypt  = require('bcrypt-nodejs')
  , crypto  = require('crypto')
  , Class   = require( 'gaz/class' )
  , Options = require( 'gaz/class/options' )
  , Parent  = require( 'gaz/class/parent' )
  , Hasher  = require('./default')
  , Bcrypt
  ;

/**
 * Description
 * @class module:bcrypt.js.Thing
 * @param {TYPE} param
 * @example var x = new bcrypt.js.THING();
 */

Bcrypt = new Class({
	inherits: Hasher
	,algorithm:'bcrypt-sha256'
	,options:{
		iterations: 6
	}
	,encode: function( password, salt){
		var hasher = crypto.createHash('sha256')
		  , data
		  ;

		hasher.update( password );
		data = bcrypt.hashSync( hasher.digest( 'hex' ) , salt );
		return util.format( "%s::%s", this.algorithm, data );
	}
	,salt: function( ){
		return bcrypt.genSaltSync( this.options.iterations )
	}
	,verify: function( password, encoded ){
		var bits = encoded.split('::')
		var algorithm = bits[0]
		var data = bits[1]

		var hasher = crypto.createHash('sha256');
		hasher.update( password )
		var pw = hasher.digest( 'hex' )

		return bcrypt.compareSync(pw, data )
	}
});

module.exports = Bcrypt;
