/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * bcrypt.js
 * @module bcrypt.js
 * @author 
 * @since 0.0.1
 */

var util = require( 'util' )
  , crypto = require('crypto')
  , Class = require( 'gaz/class' )
  , Options = require( 'gaz/class/options' )
  , Parent = require( 'gaz/class/parent' )
  , DefaultHasher
  ;

/**
 * Description
 * @class module:bcrypt.js.Thing
 * @param {TYPE} param
 * @example var x = new bcrypt.js.THING();
 */
DefaultHasher = new Class({
	mixin:[ Options, Parent ]
	,options:{}
	,constructor:function( options ){
		this.setOptions( options );
	}
	/**
	 * This does something
	 * @param {String} password description
	 * @param {String} salt description
	 **/
	,encode: function( password, salt ){
		var e = new Error();
		e.name = 'NotImplementedError'
		e.message('Default hasher must be subclassed to implement encode()')
		throw e;
	}

	/**
	 * Generates a random salt
	 * @method NAME
	 * @return {String} A random ascii string 10 bytes in size
	 **/
	,salt: function(){
		return crypto.randomBytes(10)
	}
	,verify: function(password, encoded){
		var e = new Error();
		e.name = 'NotImplementedError'
		e.message('Default hasher must be subclassed to implement verify()')
		throw e;
	}
});

module.exports = DefaultHasher;
