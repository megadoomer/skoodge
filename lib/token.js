/*jshint laxcomma: true, smarttabs: true, node: true, esnext: true*/
'use strict';
/**
 * Module for encoding and decoding time sensitif tokens
 * @module skoodge/lib/token
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires keef
 * @requires crypto
 */

var crypto = require('crypto')
  , conf   = require('keef')
  ;

const SALT = 'skoodge.lib.token.tokengenerator';
const DAY = 1000 * 60 * 60 * 24;

function make_hash(user, timestamp){
	let hasher  = crypto.createHmac( 'sha256', SALT );
	hasher.update( user.id );
	hasher.update( user.password );
	hasher.update( user.loginDate || '' );
	hasher.update( timestamp );
	return hasher.digest('hex');
}

/**
 * encodes an object into time sensitive token
 * @param {module:skoodge/models/user} user a user instance to use to generate a token
 * @param {Date} timestamp a date object representing the time from with the toek is issued
 * @return {String} token a time sensitive toekn for the spepcified user
 * @example
var token = require('skoodge/lib/token')
var User = require('skoodge/lib/token')
User.get('2b0fe11e-ad68-4a43-99a1-d8ebca8a835e')
    .then(function( user ){
		var payload = token.encode( user, new Date() ) //  inggfaxe-2004987d5b8ac01f02958ae228dceafc828ef942e56b4cbc198315ad87306186
    })
 **/ 
exports.encode = function( user, timestamp ){
	let now_b36
	  , hash;

	now_b36 = ( + timestamp ).toString(36);
	hash = make_hash( user, now_b36 );
	return `${now_b36}-${hash}` ;
};

/**
 * Checks a generated token against a user. returns true if the token validates and the lapsed time is less than the configured time
 * under auth.password_reset:timeout_days
 * @param {String} token The token to decode
 * @param {String} secret The shared secret the token was originally signed with
 * @return {Object} The decoded auth token object
 * @example
var token = require('skoodge/lib/token')
var User = require('skoodge/lib/token')
User.get('2b0fe11e-ad68-4a43-99a1-d8ebca8a835e')
    .then(function( user ){
		var payload = token.encode( user, new Date() )
		token.check( user, payload ) // true
    });
 **/
exports.check = function( user, token ){
	let bits = token.split('-')
	  , created
	  , recreated 
	  , valid
	  ;

	created   = new Date( parseInt( bits[0], 36 ) );
	recreated = encode( user, created.getTime() );
	valid     = token === recreated;
	return valid && ( ( new Date() - created / DAY ) < conf.get('auth:password:timeout_days') );
};
