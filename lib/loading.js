/*jshint laxcomma: true, smarttabs: true, node: true */
'use strict';
/**
 * Module for helper loading functions
 * @module hive-auth/lib/loading
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires hive-conf
 */

var conf = require('keef');

/**
 * Loads the configured user model
 * @function
 * @name getUserModel
 * @return {Model} configured user model at hive.auth.user.model from conf 
 **/
exports.getUserModel = function(){
	return require( conf.get('auth:user:model') )
}
