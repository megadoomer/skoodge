var initialize = require('passport/lib/middleware/initialize')

module.exports = function(){
	return{
		initialize: initialize
		,authenticate: require('./authenticate')
		,name:'megadoomer-hapi'
	}
}
