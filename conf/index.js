/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * Configuration options for alice-auth
 * @module alice-auth/conf
 * @author Eric Satterwhite
 * @since 0.1.0
 */

var path = require('path')
module.exports = {
	auth:{
		hashing:'bcrypt'
		,password:{
			timeout_days: 1
		}
		,user:{
			model: path.resolve(__dirname, '..', 'models','user')
		}
	}
}
