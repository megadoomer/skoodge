/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * Default Group object containing a collection of permissions
 * @module skoodge/models/group
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires zim/lib/db
 * @requires skoodge/models/group
 * @requires skoodge/lib/hasher/bcrypt
 */
var connection = require('zim/lib/db').connection
  , r          = connection.default.r
  , fields     = connection.default.type
  , Role
  ;

module.exports = Role = connection.default.createModel('auth_role', {
    permissions: fields.object()
},{
    pk:'name'
});
