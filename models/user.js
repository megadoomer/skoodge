/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * Base user instance for dealing with authorization
 * @module skoodge/models/user
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires zim/lib/db
 * @requires skoodge/models/group
 * @requires skoodge/lib/hasher/bcrypt
 */

var connection = require('zim/lib/db').connection
  , hasher     = new(require('../lib/hasher/bcrypt'))
  , toArray    = require('gaz/lang').toArray
  , date      = require('gaz/date')
  , Role      = require('./role')
  , get       = require('gaz/object').get
  , r          = connection.default.r
  , fields     = connection.default.type
  , User
  ;


User = connection.default.createModel('auth_user',{
	username    : fields.string().required()
    
    , created   : fields.date().default(function(){
    	return r.now()
    })

	, name      : fields.object().schema({
		first : fields.string().required()
		, last  : fields.string().required()
	})

	, email     : fields.string().email().required()
	, loginDate : fields.date().default( null )
	, superuser : fields.boolean().default( false )
	, active    : fields.boolean().default( true )
	, password  : fields.string()
	, roles     : [ fields.string() ]
},{
	pk:'auth_user_id'
});

/**
 * retuns user instances with out the password field
 * @method module:skoodge/models/user#secure
 **/
User.defineStatic('secure',function(){
	return this.without('password');
});

/**
 * returns user instances with all permissions merged into a single document 
 * @method module:skoodge/models/user#withPermissions
 **/
User.defineStatic('withPermissions', function(){
	return this.merge(function( doc ){
		return r.object(
			'permissions',
			doc('roles').eqJoin(function( id ){
			   return id
			}, r.table( Role.getTableName() ) ).default({}).zip()('permissions')
			.reduce(function(left, right ){
				return left.merge(right )
			}).default(r.object())
		)
	})
});

/**
 * Sets a new password on a user instance. The password with be salted and encoded before saved
 * @param {String} password the password to set
 * @param {String} [salt] A salt to use to create the encoded password with. One will be generated if not set
 * @chainable
 * @method module:skoodge/models/user#setPassword
 **/
User.define('setPassword', function( password, salt ){
	this.password = hasher.encode( password, salt || hasher.salt() );
	return this;
});

/**
 * Check a plain text password against the encoded password on the user instance
 * @method module:skoodge/models/user#checkPassword
 * @param {String} password the password to check
 * @return {Boolean}
 **/
User.define('checkPassword', function( txt ){
	return hasher.verify( txt, this.password );
});

/**
 * Given a permissions string, resolves a true / false if a user has the given permission.
 * A permission string represensts an object heirarchy using a colon as a property accessor
 * @method module:skoodge/models/user# perms
 * @param {String} permission The permission to check
 * @return {Promise}
 * @example
 * user.hasPerms('auth') // has any permissions in the auth module
 * user.hasPerms('auth:user') // has any user specific permssions 
 * user.hasPerms('auth:user:create') // can create a user instance
 **/
User.define("hasPerms", function( perms ){
	return new Promise(function( resolve, reject ){
		if( this.superuser ){
			return resolve( true );
		}
		this.getAllPermissions()
			.then(( p )=>{
				resolve( !!get(p, perms || '' ))
			})
	}.bind( this ));
});

/**
 * Resolves all permissions a user has 
 * @method module:skoodge/models/user#getAllPermissions
 * @param {Promise}  
 **/
User.define('getAllPermissions', function( ){
	var user_id = this.id;
	var that = this;
	return new Promise(function( resolve, reject ){
		var query = Role;


		if( that._perms ){
			return resolve( that._perms );
		}

		if( that.superuser ){
			return Role
					.getField('permissions')
					.reduce( function( left, right ){
						return left.merge( right );
					})
					.then(function( perms ){
						Object.defineProperty(that,"_perms",{
							enumerable:false,
							get: function(){
								return perms;
							}
						});
						resolve( perms );
					});
		}

		if( !that.roles || !that.roles.length ){
			return resolve( {} )
		}

		Role
			.getAll( r.args(that.roles),{index:'auth_role_id'} )
			.getField('permissions')
			.reduce(function( left, right ){
				return left.merge( right );
			})

			.then(function(perms){
				Object.defineProperty(that,"_perms",{
					enumerable:false,
					get: function(){
						return perms;
					}
				});
				resolve( perms );
			});
	});
	
});

module.exports =  User;
